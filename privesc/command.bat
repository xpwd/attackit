:: This can be used as a pseudo-command prompt when command prompt is disabled.
:: Not working:
:: 		changing directories

@echo off
echo Type commands below:
:loop
set /p unique_command_variable=""
start "" /wait /b /i cmd.exe /c %unique_command_variable%
goto loop